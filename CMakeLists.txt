cmake_minimum_required(VERSION 3.9)
project(Novo_Modelo___Edge_Coloring)

set(CMAKE_CXX_STANDARD 11)

add_executable(Novo_Modelo___Edge_Coloring
        CPP/Graph.cpp
        CPP/Grasp.cpp
        CPP/main.cpp
        CPP/Vertex.cpp
        HPP/Graph.hpp
        HPP/Grasp.hpp
        HPP/Include.hpp
        HPP/Vertex.hpp)
