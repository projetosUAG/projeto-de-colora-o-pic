#ifndef __GRAPH_HPP__
#define __GRAPH_HPP__

#include "Include.hpp"
#include "Vertex.hpp"
class Graph{
public:
    /*==========================================================================*/
    /* INITIALIZATION */
    /*==========================================================================*/

    //Creates a new graph with the edges passed in the input file
    Graph(string inputFile);
    ~Graph();

    /*==========================================================================*/
    /*CONCLUSION */
    /*==========================================================================*/

    //Prints the attributes of the graph in the terminal
    void PrintToGraph();

    friend class Grasp;
private:

    /*==========================================================================*/
    /* DEVELOPMENT */
    /*==========================================================================*/

    //Search and return if there is a new vertex or create and return if it does not exist
    Vertex* SearchVertex(int name);

    //Checks if the graph was completely colored
    bool Colorful();

    // Arrange the graph in ascending order in relation to the degree of the vertex
    void OrganizeToGraph();

    // Arrange each vertex of the vertex vector of the graph at random
    // Arrange each individual edge in relation to each vertex in a random way
    void ReorganizeToGraph();

    // Modify the color of the edge between the start and end vertices by the color passed as a parameter
    void setEdge(Vertex* start, Vertex* end, int color);

    //Modifies the colors (c1 -> c2) and (c2 -> c1)
    void ChangeColors(int c1, int c2);

    //It counts the colors and after that the sum of each color
    void Sum();

    // Add a new random color representing a newly placed color in the graph
    void AddNewColorHexadecimal();

    // Print a graph template for software "xdot" in a file
    // with the "FilenameEntry + _Saida.dat"
    void RecordToFile();
    void RecordToFileArbor();


    string saida;
    vector<Vertex* > vertex; //Stores the vertex pointers of the graph
    vector<int > addedColors; //Stores to the colors added in the graph
    vector<int > quantAddedColors; //Stores the individual amount of colors in the graph
    vector<string > colorHexaAddeds;

    int dispersion; //Saves the dispersion of the graph
    float runTimeTot; //Saves the total time of the graph
    int dualLimit;
    int numberOfEdges; //Saves the number of edges of the graph
    int bestSummation; //Saves the result of the best minimization of the graph
    double gap;
    int GM;
    double complexity;
};


#endif //__GRAPH_HPP__
