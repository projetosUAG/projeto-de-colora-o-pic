#ifndef __VERTEX_HPP__
#define __VERTEX_HPP__

#include "Include.hpp"

class Vertex{
public:
    /*==========================================================================*/
    /* INITIALIZATION */
    /*==========================================================================*/

    // Initializes the variable name with the value passed as parameter
    Vertex(int name);

    /*==========================================================================*/
    /* DEVELOPMENT */
    /*==========================================================================*/

    // Add an edge between the vertex that called the function and the vertex passed as function parameter
    void AddEdge(Vertex* end, int color);

    //Verifica se o grafo contém a cor designada
    //bool ContainsColor(int color);

    //Changes the order of the vector of edges in descending order according to the degree of color
    void Organize();

    void Catalog();

    //Changes the array order of edges randomly
    void OrganizeAleatory();

    // Returns next valid color
    int getColorValidated(int current);

    void setEdge(Vertex* end, int color);

    // Returns to the other end of the edge
    //int getOtherEdgeTip(int end);

    bool Verify();

    /*==========================================================================*/
    /*CONCLUSION */
    /*==========================================================================*/

    friend class Graph;
    friend class Grasp;
private:
    int name;
    int colored;
    bool visited;
    int colorValidated;
    vector<int > catalogVertex;
    vector<Vertex* > endVertex;
    vector<unsigned int > edgeColor;
    vector<bool > qColor;

};

#endif //__VERTEX_HPP__
