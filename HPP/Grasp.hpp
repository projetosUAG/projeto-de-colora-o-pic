#ifndef __GRASP_HPP__
#define  __GRASP_HPP__

#include "Include.hpp"
#include "Graph.hpp"

class Grasp{
public:
    /*==========================================================================*/
    /* INITIALIZATION */
    /*==========================================================================*/

    //Starts the staining of the last graph and the input parameters
	Grasp(Graph* graph, int time, int alpha, double porcent, string outFile);

private:
    /*==========================================================================*/
    /* DEVELOPMENT */
    /*==========================================================================*/

    //Method responsible for Grasp startup and execution
    void GraspInitialize();

    //Method responsible for obtaining a dual limit of the problem for a later comparison
    void DualLimit();

    //Method responsible for building a solution from scratch
    void Constructor();

    //Method responsible for the improvement of the graph (Local Search)
    void LocalSearch();

    //Method part of Local Search
    void LocalSearchAddColor(Vertex* start, Vertex* end);

    // Method responsible for checking for other possibility of edge changes
    // adjacent to the vertices that have undergone later modifications and if the exchange
    // do not improve the summation then reverse the original color again
    void PossibleExchangeVertex(Vertex* v);

    // Function responsible for checking the possibility of making a color change
    // between three vertices that makes it possible to minimize the total color of the graph
    pair<int, int> PossibleExchange(Vertex* start);

    //Method responsible for making the change of colors between three vertices
    void TripleTrouble(Vertex* start, int end, int vAux);

    //Function responsible for trying to make an improvement in vertices of the triple exchange
    bool SeekImprove(Vertex* start);

    //Organizes and does graph color switching
    bool CheckColorChange();

    void SearchBestColor(Vertex* start, Vertex* end);

    //Checks if the vertex breaks some condition
    bool Verify();

    //Checks if the vertex breaks some condition
    bool VerifyComplet();

    void getResultTable();
    
    void PrintResultTable(string file);

    /*==========================================================================*/
    /*CONCLUSION */
    /*==========================================================================*/

    //Prints the result of the graph
    void Print();


    vector<Vertex* > forbidden;
    vector<Vertex* > authorized;

    Graph* graph;
    string resultTable;
    double runtime;
    double runtimeGRASP;
    double t0;
    double gapStop;
    double alpha;
    int time;
    int maxPrevious = 2147483647;
};

#endif // __GRASP_HPP__
