#ifndef INCLU_HPP
#define INCLU_HPP
  //Rm818

  #include <stdio.h>
  #include <stdlib.h>     /* atoi */
  #include <iostream>   // std::cout
  #include <string>     // std::string, std::stoi
  #include<sstream>
  #include <vector>
  #include <fstream>
  #include <bits/time.h>
  #include <algorithm> // sort
  using namespace std;

  const string cacmText("|=========================================|\n"
                        "|=========================================|\n"
                        "      ____       _        ____    __  __ \n"
                        "     / ___|     / \\      / ___|  |  \\/  |\n"
                        "    | |        / _ \\    | |      | |\\/| |\n"
                        "    | |___    / ___ \\   | |___   | |  | |\n"
                        "     \\____|  /_/   \\_\\   \\____|  |_|  |_|\n"  
                        "|=========================================|\n"
                        "|=========================================|\n");

#endif /* INCLU_HPP */