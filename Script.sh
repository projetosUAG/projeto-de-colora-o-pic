#!/bin/bash

for i in `ls *.mtx`
do

	file=$i"_log"
	if [ ! -d "_log" ]; then
		mkdir _log
	fi
	if [ -e "_log/"$file ] ; then
		echo "o arquivo "$file" existe, ignorando!"

	else
		echo "o arquivo "$file" não existe, iniciando!"
		echo "Iniciando "$file
		./MSCE.bin $i 5000 0 > "_log/"$i"_log"
		echo "Fim do "$file
		echo
		echo
	fi
done
