#include "../HPP/Include.hpp"
#include "../HPP/Grasp.hpp"

int main(int argc, char** argv){
    srand ((int)time(0));
    bool d = false;
    bool s = false;
    int time = 10;
    int alpha = 50;
    double porcent = 0;
    Graph* graph;
    Grasp* grs;
    string outFile = "";
    if(argc == 2 and argv[1][0] == '-' and argv[1][1] == 'h'){
        cout<<"================================================="<<endl;
        cout<<"================================================="<<endl;
        cout<<cacmText<<endl;
        cout<<"================================================="<<endl;
        cout<<"EXAMPLE OF INITIALIZATION WITH CORRECT PARAMETERS"<<endl;
        cout<<"-------------------------------------------------"<<endl<<endl;
        cout<<"./CACM.bin -f <string>  -o <string> -t <int> -a <int> -p <double>"<<endl<<endl;
        cout<<"Note: The parameters may be out of order shown above"<<endl<<endl;
        cout<<"-------------------------------------------------"<<endl;
        cout<<"-f: (file)    Input file with the data of the graph.\n"
              "-t: (0 ~ inf) Time limit for GRASP.\n"
              "-a: (0 ~ 100) Randomness of the constructive GRASP.\n"
              "-p: (0 ~ 100) Percentage of stop for the GAP.\n"
              "-o: (outFile) Redirect the output in TEX format.\n";
        cout<<"================================================="<<endl;
        cout<<"================================================="<<endl;
        return 0;

    }else if(argc >= 3 and argv[1][0] == '-' and argv[1][1] == 'f'){
        int arg = 3;
        while(arg < argc){
            if(argv[arg][1] == 'd'){
                d = true;
            }
            if(argv[arg][1] == 'o' and argv[arg+1][0] != '-'){
                outFile = argv[arg+1];
                s = true;
                arg++;
            }
            if(argv[arg][1] == 't' and argv[arg+1][0] != '-'){
                time = stoi(argv[arg+1]);
                arg++;
            }
            if(argv[arg][1] == 'p' and argv[arg+1][0] != '-'){
                porcent = stof(argv[arg+1]);
                arg++;
            }
            if(argv[arg][1] == 'a' and argv[arg+1][0] != '-'){
                alpha = stoi(argv[arg+1]);
                arg++;
            }arg++;
        }
        graph = new Graph(argv[2]);
        grs = new Grasp(graph, time, alpha, porcent, outFile);
        if(d){
            system(((string)("xdot ") + argv[2] + (string)("_Saida.dat -f circo")).c_str());
        }
    }else{
        cout<<"\n\nPARAMETERS INVALID, PLEASE CHECK!\n\n";
    }

    delete graph;
    delete grs;
    return 1;
}

