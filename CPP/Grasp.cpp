#include "../HPP/Grasp.hpp"

//Starts the staining of the last graph and the input parameters
Grasp::Grasp(Graph* graph, int time, int alpha, double porcent, string outFile){
    t0 = clock_t();
    cout<<cacmText<<endl;
    this->graph = graph;
    this->alpha = alpha;
    this->time = time;
    this->gapStop = porcent*10;
    this->GraspInitialize();
    this->runtime = (clock() - t0)/CLOCKS_PER_SEC;
    if(outFile.size() > 1) this->PrintResultTable(outFile);
    this->Print();
}

    //Method responsible for Grasp startup and execution
void Grasp::GraspInitialize(){
    bool stop = false;
    int quantExec = 0;
    this->DualLimit();
    double bestContAuxRepet = this->maxPrevious;
    double previousGap = -1;
    double timeAux = clock();
    int contAuxRepet = 0;
    Constructor();
    while((clock() - timeAux)/CLOCKS_PER_SEC < this->time){
        LocalSearch();
        this->graph->Sum();
        this->runtimeGRASP = (clock() - t0)/CLOCKS_PER_SEC;
        if(this->graph->gap < bestContAuxRepet){
            bestContAuxRepet = this->graph->gap;
            contAuxRepet = 0;
            //this->getResultTable();
            //this->Print();
            //this->graph->RecordToFileArbor();
            //this->graph->RecordToFile();
            if(this->graph->gap < this->gapStop) break;
        }else if(bestContAuxRepet == this->graph->gap or previousGap == this->graph->gap){
            contAuxRepet++;
        }else{
            contAuxRepet = 0;
        }
        if(contAuxRepet >= 5){
            contAuxRepet = 0;
            this->graph->ReorganizeToGraph();
            Constructor();
        }
        previousGap = this->graph->gap;
        quantExec++;
    }
    /*
    if(Verify()){
        cout<<"OK"<<endl;
    }else{
        cout<<"NOT OK"<<endl;
    }
     */
    cout<<"Quantidade de Interações: "<<quantExec<<endl;
    graph->Sum();
    this->runtimeGRASP = (clock() - timeAux)/CLOCKS_PER_SEC;
}

//Method responsible for obtaining a dual limit of the problem for a later comparison
void Grasp::DualLimit() {
    this->graph->dualLimit = 0;
    this->graph->GM = 0;
    int theta = this->graph->vertex.size()%2;
    for(auto &v : this->graph->vertex) {
        if (v->endVertex.size() > this->graph->GM) this->graph->GM = v->endVertex.size();
        int grau = v->endVertex.size();
        this->graph->dualLimit += (grau * (grau + 1));
    }
    this->graph->dualLimit = (this->graph->dualLimit/4);
}

//Color all edges of the graph
void Grasp::Constructor(){
    for(auto &v : this->graph->vertex){
        v->visited = false;
        while(v->qColor.size() < (this->graph->GM*1.5)){
            v->qColor.push_back(false);
        }
    }
    for(auto &v : this->graph->vertex){
        int vfSize = v->endVertex.size();
        for(int vf = 0; vf < vfSize; vf++){
            if(v->edgeColor[vf] == 0){
                vector<int > colorPossible;
                Vertex* auxVertex = v;
                if(v->endVertex.size() < v->endVertex[vf]->endVertex.size()) auxVertex = v->endVertex[vf];
                int auxVertexEndVertexSize = auxVertex->endVertex.size();
                for(int colorAux = 0; colorAux < auxVertexEndVertexSize; colorAux++){
                    if(!v->qColor[colorAux+1] and !v->endVertex[vf]->qColor[colorAux+1]){
                        colorPossible.push_back(colorAux+1);
                    }
                }
                int interval = ((double)colorPossible.size() / 100.0) * this->alpha;
                int position = rand()%interval;
                if(interval == 0){
                    LocalSearchAddColor(v, v->endVertex[vf]);
                }else{
                    this->graph->setEdge(v, v->endVertex[vf], colorPossible[position]);
                }
            }
        }
    }
}

//Method responsible for the improvement of the graph (Local Search)
void Grasp::LocalSearch() {
    this->graph->OrganizeToGraph();
    CheckColorChange();
    for (auto &v : graph->vertex) {
        v->visited = false;
    }
    for (auto &v : graph->vertex) {
        PossibleExchangeVertex(v);
    }
}

//Adds an initial color to a newly created edge
void Grasp::LocalSearchAddColor(Vertex* start, Vertex* end){
    if(start->edgeColor[start->catalogVertex[end->name]] == 0 and end->edgeColor[end->catalogVertex[start->name]] == 0){
        int newColor = 1;
        while(!(!start->qColor[newColor] and !end->qColor[newColor])){
            newColor++;
        }
        this->graph->setEdge(start, end, newColor);
    }
}

// Method responsible for checking for other possibility of edge changes
// adjacent to the vertices that have undergone later modifications and if the exchange
// do not improve the summation then reverse the original color again
void Grasp::PossibleExchangeVertex(Vertex* v){
    pair<int, int > poss = PossibleExchange(v);
    if(poss.first != -1 and poss.second != -1){
        TripleTrouble(v, poss.first, poss.second);
        //SeekImprove(v);
        //SeekImprove(v->endVertex[poss.first]);
        SeekImprove(v->endVertex[poss.first]->endVertex[poss.second]);
    }
    v->visited = true;
}

// Function responsible for checking the possibility of making a color change
// between three vertices that makes it possible to minimize the total color of the graph
pair<int, int> Grasp::PossibleExchange(Vertex* start){
    pair<int, int> ret;
    ret.first = -1;
    ret.second = -1;
    if(!start->visited){
        start->visited = true;
        int startEndVertexSize = start->endVertex.size();
        for(int vf = 0; vf < startEndVertexSize; vf++){
            Vertex* end = start->endVertex[vf];
            int vAuxSize = end->endVertex.size();
            for(int vAux = 0; vAux < vAuxSize; vAux++){
                // and !start->qColor[end->edgeColor[vAux]]
                if(!end->endVertex[vAux]->qColor[start->edgeColor[vf]] and !start->qColor[end->edgeColor[vAux]]){
                    ret.first = vf;
                    ret.second = vAux;
                    break;
                }
            }
        }
    }
    return ret;
}

//Method responsible for making the change of colors between three vertices
void Grasp::TripleTrouble(Vertex* start, int end, int vAux){
    int c1 = start->edgeColor[end];
    int c2 = start->endVertex[end]->edgeColor[vAux];

    Vertex* vAuxT = start->endVertex[end]->endVertex[vAux];

    start->setEdge(start->endVertex[end], c2);
    start->endVertex[end]->setEdge(start, c2);

    start->endVertex[end]->setEdge(vAuxT, c1);
    vAuxT->setEdge(start->endVertex[end], c1);
}


//Function responsible for trying to make an improvement in vertices of the triple exchange
bool Grasp::SeekImprove(Vertex* startVertex){
    bool changed = false;
    if(!startVertex->visited){
        for (auto end : startVertex->endVertex) {
            int bestAux = end->colorValidated;
            int startEdgeColorSize = end->edgeColor.size();
            for(int c = 0; c < startEdgeColorSize; c++){
                int colorAux = end->edgeColor[c];
                if(bestAux < end->edgeColor[c]){
                    int endColor = end->edgeColor[end->catalogVertex[end->endVertex[c]->name]];
                    int newColor = 1;
                    while(!(!end->qColor[newColor] and !end->endVertex[c]->qColor[newColor])){
                        if(newColor == endColor) break;
                        newColor++;
                    }if(newColor < endColor){
                        end->setEdge(end->endVertex[c], newColor);
                        end->endVertex[c]->setEdge(end, newColor);
                    }
                    if(end->edgeColor[c] < colorAux){
                        changed = true;
                        bestAux = end->colorValidated;
                    }
                }
            }
        }
        startVertex->visited = true;
    }
    return changed;
}


//Organizes and does graph color switching
bool Grasp::CheckColorChange(){
    bool ret = false;
    for (auto &c : graph->vertex) {
        for (int vf : c->edgeColor) {
            graph->quantAddedColors[vf -1]++;
        }
    }
    int graphAddedColorsSize = graph->addedColors.size();
    for(int c1 = 0; c1 < graphAddedColorsSize; c1++){
        for(int c2 = c1+1; c2 < graphAddedColorsSize; c2++){
            int q1c1 = graph->addedColors[c1]*graph->quantAddedColors[c1];
            int q1c2 = graph->addedColors[c1]*graph->quantAddedColors[c2];
            int q2c1 = graph->addedColors[c2]*graph->quantAddedColors[c1];
            int q2c2 = graph->addedColors[c2]*graph->quantAddedColors[c2];
            if(q1c2 + q2c1 <= q1c1 + q2c2){
                graph->ChangeColors(graph->addedColors[c1], graph->addedColors[c2]);
                int aux = graph->quantAddedColors[c1];
                graph->quantAddedColors[c1] = graph->quantAddedColors[c2];
                graph->quantAddedColors[c2] = aux;
                ret = true;
            }
        }
    }
    return ret;
}

//Checks if the vertex breaks some condition
bool Grasp::Verify(){
    for (auto &i : graph->vertex) {
        if(!i->Verify()) return false;
    }
    return true;
}

//Checks if the vertex breaks some condition
bool Grasp::VerifyComplet(){
    for (auto &i : graph->vertex) {
        int iEdgeColorSize = i->edgeColor.size();
        for (int j = 0; j < iEdgeColorSize; j++) {
            for (int k = j+1; k < iEdgeColorSize; k++) {
                if((j != k and (i->edgeColor[j] == i->edgeColor[k] or i->endVertex[j]->edgeColor[i->endVertex[j]->catalogVertex[i->name]] != i->edgeColor[j]))){
                    return false;
                }
            }
        }
    }
    return true;
}

void Grasp::PrintResultTable(string file) {
    ofstream out; // out é uma variavel.
    out.open(file.c_str(), ios_base::app);
    out << this->resultTable<<endl;
    out.close(); // não esqueça de fechar... XD
}

void Grasp::getResultTable() {
    string resultStringAux = "    ";
    resultStringAux = resultStringAux + this->graph->saida.c_str();
    resultStringAux = resultStringAux + " & ";
    resultStringAux = resultStringAux + to_string(this->graph->vertex.size());
    resultStringAux = resultStringAux + " & ";
    resultStringAux = resultStringAux + to_string(this->graph->numberOfEdges);
    resultStringAux = resultStringAux + " & ";
    resultStringAux = resultStringAux + to_string(this->graph->dispersion);
    resultStringAux = resultStringAux + " & ";
    resultStringAux = resultStringAux + to_string(this->graph->bestSummation);
    resultStringAux = resultStringAux + " & ";
    resultStringAux = resultStringAux + to_string(this->graph->addedColors.size());
    resultStringAux = resultStringAux + " & ";
    resultStringAux = resultStringAux + to_string(this->runtimeGRASP);
    resultStringAux = resultStringAux + " & ";
    resultStringAux = resultStringAux + to_string(this->graph->gap);
    resultStringAux = resultStringAux + " & ";
    resultStringAux = resultStringAux + to_string(this->graph->dualLimit);
    resultStringAux = resultStringAux + " \\";
    resultStringAux = resultStringAux + "\\ ";
    this->resultTable = resultStringAux;
}

//Printe the current graph
void Grasp::Print(){
    graph->PrintToGraph();
    cout<<"Alpha      : "<<this->alpha<<"%"<<endl;
    cout<<"Time  GRASP: "<<runtimeGRASP<<" seg"<<endl;
    cout<<"Runtime TOT: "<<runtime<<" seg"<<endl;
    cout<<"|=========================================|"<<endl;
}
