#include "../HPP/Graph.hpp"

//Creates a new graph with the edges passed in the input file
Graph::Graph(string inputFile){
    string line;
    vector<string> parts;
    this->saida = inputFile;
    ifstream filetoRead (inputFile.c_str()); // ifstream = padrão ios:in
    string tamanho;
    getline(filetoRead,tamanho);
    if (filetoRead.is_open()){
        while (! filetoRead.eof()){ //Until the end of the file
            line = "";
            getline(filetoRead,line); // como foi aberto em modo texto(padrão) e não binário(ios::bin) pega cada linha
            if(line != ""){
                string part1, part2;
                bool found = false;
                for(char &i : line){
                    if(!found and i == ' '){
                        found = true;
                    }else{
                        if(!found){
                            part1.push_back(i);
                        }else{
                            part2.push_back(i);
                        }
                    }
                }
                Vertex* v1 = SearchVertex(atoi(part1.c_str()));
                Vertex* v2 = SearchVertex(atoi(part2.c_str()));
                bool flagSTOP = false;
                for (auto &i : v1->endVertex) {
                    if(i->name == v2->name){
                        flagSTOP = true;
                        break;
                    }
                }
                if(!flagSTOP){
                    for (auto &j : v2->endVertex) {
                        if(j->name == v1->name){
                            flagSTOP = true;
                            break;
                        }
                    }
                }
                if(!flagSTOP){
                    v1->AddEdge(v2, 0);
                    v2->AddEdge(v1, 0);
                    this->numberOfEdges++;
                }
            }
        }
        filetoRead.close();
    }
    int qMax = 0;
    int graphVertexSize = this->vertex.size();
    for(int i = 2; i < graphVertexSize + 1; i++){
        qMax += i-1;
    }
    this->dispersion = (100 - ((this->numberOfEdges * 100.0) / qMax));
    this->OrganizeToGraph();
}

Graph::~Graph() {
    for(auto &v : this->vertex){
        delete v;
    }
}

//Search for an existing vertex, return if it exists or create and return if not
Vertex* Graph::SearchVertex(int name){
    for (auto &i : vertex) {
        if(i->name == name){
            return i;
        }
    }
    Vertex *newVer = new Vertex(name);
    vertex.push_back(newVer);
    return newVer;
}

//Checks if the graph was completely colored
bool Graph::Colorful(){
    for (auto &v : this->vertex) {
        if(v->qColor[0]){
            return false;
        }
    }
    return true;
}

// Arrange the graph in ascending order in relation to the degree of the vertex
void Graph::OrganizeToGraph() {
    int graphVertexSize = this->vertex.size();
    for (int i = 0; i < graphVertexSize; i++) {
        int prox = i;
        for (int j = i + 1; j < graphVertexSize; j++) {
            if (this->vertex[j]->endVertex.size() < this->vertex[prox]->endVertex.size()) {
                prox = j;
            }
        }
        Vertex* aux = this->vertex[prox];
        this->vertex[prox] = this->vertex[i];
        this->vertex[i] = aux;
    }
    for (auto &v : this->vertex) {
        v->Organize();
    }
}

// Arrange each vertex of the vertex vector of the graph at random
// Arrange each individual edge in relation to each vertex in a random way
void Graph::setEdge(Vertex* start, Vertex* end, int color){
    start->setEdge(end, color);
    end->setEdge(start, color);
    while(color > quantAddedColors.size()){
        addedColors.push_back(0);
        quantAddedColors.push_back(0);
        AddNewColorHexadecimal();
    }
}

// Modify the color of the edge between the start and end vertices by the color passed as a parameter
void Graph::ReorganizeToGraph(){
    int graphVertexSize = this->vertex.size();
    for(auto &v : this->vertex){
        int vEdgeColorSize = v->edgeColor.size();
        for(int vf = 0; vf < vEdgeColorSize; vf++){
            v->setEdge(v->endVertex[vf], 0);
        }
    }
    int max = graphVertexSize/2;
    for(unsigned int p = 0; p < max; p++){
        int cur = rand()%(graphVertexSize-1);
        Vertex* vAux = this->vertex[p];
        this->vertex[p] = this->vertex[cur];
        this->vertex[cur] = vAux;
    }
    for(auto &v : this->vertex){
        v->OrganizeAleatory();
    }
    this->bestSummation = 0;
}

//Modifies the colors (c1 -> c2) and (c2 -> c1)
void Graph::ChangeColors(int c1, int c2){
    for(auto &v : this->vertex) v->visited = false;
    for(auto &v : this->vertex){
        if(!v->visited){
            v->visited = true;
            if(v->qColor[c1] or v->qColor[c2]) {
                int vEndVertexSize = v->endVertex.size();
                for(int vf = 0; vf < vEndVertexSize; vf++){
                    if(v->edgeColor[vf] == c1){
                        v->setEdge(v->endVertex[vf], c2);
                    }else if(v->edgeColor[vf] == c2){
                        v->setEdge(v->endVertex[vf], c1);
                    }
                }
            }
        }
    }
}

//It counts the colors and after that the sum of each color
void Graph::Sum(){
    int summ = 0;
    int newQuantColors = 0;

    for(auto &vertex : this->vertex){
        for(unsigned int &color : vertex->edgeColor){
            summ += color;
            if(color > newQuantColors) newQuantColors = color;
        }
    }
    this->bestSummation = summ/2;
    this->gap = -((((double)this->dualLimit / this->bestSummation) * 100)-100);
    while(this->addedColors.size() > newQuantColors) addedColors.pop_back();
}

// Add a new random color representing a newly placed color in the graph
void Graph::AddNewColorHexadecimal(){
    string partsHEXA_FULL[16] = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
    string partOCTA_P[8] = {"0","2","4","6","8","A","C","E"};
    string partOCTA_I[8] = {"1","3","5","7","9","B","D","F"};
    string newColorHexa;
    bool notStop = true;
    while(notStop){
        newColorHexa = "\"#";
        //Color Hexa_FULL
        for (int grb = 0; grb < 2; grb++) {
            int partsColorHexa = rand()%16;
            newColorHexa += partsHEXA_FULL[partsColorHexa];
        }
        //Color Octa_P
        for (int grb = 0; grb < 2; grb++) {
            int partsColorHexa = rand()%8;
            newColorHexa += partOCTA_P[partsColorHexa];
        }
        //Color Octa_I
        for (int grb = 0; grb < 2; grb++) {
            int partsColorHexa = rand()%8;
            newColorHexa += partOCTA_I[partsColorHexa];
        }
        newColorHexa += "\"";
        if(newColorHexa.size() == 9){
            bool contains = false;
            for (const auto &colorHexaAdded : colorHexaAddeds) {
                if(colorHexaAdded == newColorHexa){
                    contains = true;
                    break;
                }
            }if(!contains){
                colorHexaAddeds.push_back(newColorHexa);
                notStop = false;
            }
        }
    }
}

//Record the edges with their correct colors on file
void Graph::RecordToFile(){
    //cout<<"Initializing Recording to File"<<endl;
    bool flagBLACK = false;
    int total = 0;
    this->Sum();
    vector<int > control;
    string outFile = this->saida  + "_Saida.dat";
    ofstream out; // out é uma variavel.
    out.open(outFile.c_str()); // o arquivo que será criado;
    out<<"graph G{"<<endl;
    out<<"	rankdir=V;"<<endl;
    out<<"	size=\"7\""<<endl;
    out<<"	node [shape = circle];"<<endl;
    out<<"	Somatório_Mínimo -- "<<this->bestSummation<<";"<<endl;
    for(auto &i : vertex){
        if(!flagBLACK){
            control.push_back(i->name);
            int iEndVertexSize = i->endVertex.size();
            for(int j = 0; j < iEndVertexSize; j++){
                if(!flagBLACK){
                    int contains = 0;
                    for (int k : control) {
                        if(i->endVertex[j]->name == k){
                            contains = 1;
                            break;
                        }
                    }
                    if(contains != 1){
                        out<<"	v"<<i->name<<" -- v"<<i->endVertex[j]->name<<" [ label = "<<i->edgeColor[j]<<" color = "+ colorHexaAddeds[(i->edgeColor[j])-1] + "];"<<endl;
                        total += i->edgeColor[j];
                    }
                }else{
                    break;
                }
            }
        }else{
            break;
        }
    }
    if(flagBLACK){
        bestSummation = -1;
        colorHexaAddeds.clear();
        colorHexaAddeds.swap(colorHexaAddeds);
    }
    //out<<"}";
    out.close(); // não esqueça de fechar... XD
    // remove("Saida.dat");
    control.clear();
    control.swap(control);
    //cout<<"Finalizing Recording to File"<<endl;
}

//Record the edges with their correct colors on file
void Graph::RecordToFileArbor(){
    //cout<<"Initializing Recording to File"<<endl;
    bool flagBLACK = false;
    int total = 0;
    this->Sum();
    vector<int > control;
    string outFile = this->saida  + "_Arbor.dat";
    ofstream out; // out é uma variavel.
    out.open(outFile.c_str()); // o arquivo que será criado;
    out<<"{color:#000000}"<<endl;
    for(auto &i : vertex){
        if(!flagBLACK){
            control.push_back(i->name);
            int iEndVertexSize = i->endVertex.size();
            for(int j = 0; j < iEndVertexSize; j++){
                if(!flagBLACK){
                    int contains = 0;
                    for (int k : control) {
                        if(i->endVertex[j]->name == k){
                            contains = 1;
                            break;
                        }
                    }
                    if(contains != 1){
                        out<<"v"<<i->name<<" -- v"<<i->endVertex[j]->name<<"{color: "<<colorHexaAddeds[(i->edgeColor[j])-1]<<"}"<<endl;
                    }
                }else{
                    break;
                }
            }
        }else{
            break;
        }
    }
    out.close();
    control.clear();
    control.swap(control);
}

//Printe the current graph
void Graph::PrintToGraph(){
    cout<<endl<<"==============================="<<endl;
    cout<<"   Printing Graph Properties   "<<endl;
    cout<<"==============================="<<endl<<endl;
    cout<<"Number of Vertex: "<<this->vertex.size()<<endl;
    cout<<"Number of Edges: "<<this->numberOfEdges<<endl;
    cout<<"Number of Colors: "<<this->addedColors.size()<<endl;
    cout<<"Complexity of the Graph: "<<this->complexity<<endl;
    cout<<"Dispersion of the graph: "<<this->dispersion<<"%"<<endl;
    cout<<"Dual Sum: "<<this->dualLimit<<endl;
    cout<<"GAP: "<<this->gap<<"%"<<endl;
    cout<<"GM: "<<this->GM<<endl;
    cout<<"Less sum of colors: "<<this->bestSummation<<endl<<endl;
    /*
    for(auto v : vertex){
        cout<<"["<<v->name<<"] ->    ";
        for(auto vf : v->endVertex){
            cout<<" ["<<vf->name<<" : "<<v->edgeColor[v->getOtherEdgeTip(vf->name)]<<"]";
        }
        cout<<endl;
    }
     */
    cout<<"==============================="<<endl;
}
