#include "../HPP/Vertex.hpp"

/*==========================================================================*/
/* INITIALIZATION */
/*==========================================================================*/

//Initializes the variable name with the value passed as parameter
Vertex::Vertex(int name){
    this->name = name;
}

/*==========================================================================*/
/* DEVELOPMENT */
/*==========================================================================*/

// Add an edge between the vertex that called the function and the vertex passed as function parameter
void Vertex::AddEdge(Vertex* end, int color){
    endVertex.push_back(end);
    edgeColor.push_back(color);
    this->colored = -1;
}
void Vertex::Organize(){
    int endVertexSize = this->endVertex.size();
    for (int i = 0; i < endVertexSize; i++) {
        int prox = i;
        for (int j = i; j <endVertexSize; j++) {
            if (this->edgeColor[j] > this->edgeColor[prox]) {
                prox = j;
            }
        }
        Vertex* aux = this->endVertex[prox];
        int color = this->edgeColor[prox];

        this->endVertex[prox] = this->endVertex[i];
        this->edgeColor[prox] = this->edgeColor[i];
        this->endVertex[i] = aux;
        this->edgeColor[i] = color;
    }
    this->Catalog();
}

void Vertex::Catalog() {
    for(int v = 0; v < this->endVertex.size(); v++){
        while(this->catalogVertex.size() <= this->endVertex[v]->name) this->catalogVertex.push_back(0);
        this->catalogVertex[this->endVertex[v]->name] = v;
    }
}

void Vertex::OrganizeAleatory() {
    vector<Vertex* > vecAux;

    //Guardando os ponteiros na ordem atual
    int endVertexSize = this->endVertex.size();
    for(int vf = 0; vf < endVertexSize; vf++){
        vecAux.push_back(this->endVertex[vf]);
    }

    //Limpando o vetor endVertex para inserir aleatório
    this->endVertex.clear();
    this->endVertex.swap(this->endVertex);

    while(vecAux.size() > 0){
        int cur = rand()%(vecAux.size());
        Vertex* a1 = vecAux[cur];
        vecAux[cur] = vecAux[vecAux.size()-1];
        vecAux[vecAux.size()-1] = a1;

        this->endVertex.push_back(a1);

        vecAux.pop_back();
    }
    this->Catalog();
}

// Returns next valid color
int Vertex::getColorValidated(int current){
    int prox = current + 1;
    int edgeColorSize = this->edgeColor.size();
    for(int i = 0; i  < edgeColorSize; i++){
        for (int j : this->edgeColor) {
            if(j == prox){
                prox += 1;
                break;
            }
        }
    }
    return prox;
}

/*
// Returns to the other end of the edge
int Vertex::getOtherEdgeTip(int end){
    return this->catalogVertex[end];
}
*/
bool Vertex::Verify() {
    int edgeColorSize = this->edgeColor.size();
    for (int j = 0; j < edgeColorSize; j++) {
        for (int k = j+1; k < edgeColorSize; k++) {
            if(j != k or this->edgeColor[j] != 0){
                if(this->edgeColor[j] == this->edgeColor[k] or this->edgeColor[j] != this->endVertex[j]->edgeColor[this->endVertex[j]->catalogVertex[this->name]]){
                    return false;
                }
            }else return false;
        }
    }
    return true;
}

void Vertex::setEdge(Vertex* end, int color) {
    while(qColor.size() <= color) this->qColor.push_back(false);
    int pos = this->catalogVertex[end->name];
    if(this->edgeColor[pos] == color) return;
    int colorAux = this->edgeColor[pos];

    this->edgeColor[pos] = color;

    this->qColor[color] = true;
    this->qColor[colorAux] = false;

    for(int c : this->edgeColor){
        if(c == colorAux){
            this->qColor[c] = true;
            break;
        }
    }

    if(color == this->colorValidated){
        int prox = 1;
        bool stop = false;
        int edgeColorSize = this->edgeColor.size();
        for(int i = 0; i  < edgeColorSize; i++){
            if(!stop){
                for (int j : this->edgeColor) {
                    if(!stop and j == prox){
                        prox += 1;
                        stop = true;
                    }else if(stop){
                        break;
                    }
                }
            }else if(stop){
                break;
            }
        }
        this->colorValidated = prox;
    }
}
