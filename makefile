all: main.o Vertex.o Graph.o Grasp.o
	g++ OBJECT/main.o OBJECT/Vertex.o OBJECT/Graph.o OBJECT/Grasp.o -o CACM.bin

main.o:CPP/main.cpp
	g++ -std=c++11 -c -O3 CPP/main.cpp -o OBJECT/main.o

Vertex.o: CPP/Vertex.cpp
	g++ -std=c++11 -c -O3 CPP/Vertex.cpp -o OBJECT/Vertex.o

Graph.o: CPP/Graph.cpp
	g++ -std=c++11 -c -O3 CPP/Graph.cpp -o OBJECT/Graph.o

Grasp.o: CPP/Grasp.cpp
	g++ -std=c++11 -c -O3 CPP/Grasp.cpp -o OBJECT/Grasp.o

clear:
	rm CACM.bin OBJECT/*.o callgrind.out*
